FROM ubuntu

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    python3.8 \
    python3-pip \
    firefox \
    wget \
    && apt-get clean

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz -O /tmp/geckodriver.tar.gz \
    && tar -C /opt -xzf /tmp/geckodriver.tar.gz \
    && chmod 755 /opt/geckodriver \
    && ln -fs /opt/geckodriver /usr/bin/geckodriver \
    && ln -fs /opt/geckodriver /usr/local/bin/geckodriver

COPY requirements.txt /tmp
RUN pip install --no-cache-dir -r /tmp/requirements.txt

RUN useradd -m autify
RUN apt-get install nano

WORKDIR /home/autify

COPY fetcher/fetcher.py fetcher/
COPY fetch.sh .
RUN chown autify:autify -R /home/autify

USER autify

CMD ["tail", "-f", "/dev/null"]