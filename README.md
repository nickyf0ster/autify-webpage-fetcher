# Autify Webpage Fetcher

Webpage Fetcher allows you to easily retrieve any webpage on the internet!
Moreover, you can save fetched data for later local usage.

# Usage
### Save page HTML locally

`python3 fetcher/fetcher.py <URL>`

### Get page metadata

`python3 fetcher/fetcher.py <URL> --metadata`


# Build Docker & Run
`docker build . -t autify-webpage-fetcher`

`docker run -d autify-webpage-fetcher`

## GitLab CI
- Linter stage runs on every commit.
- Test stage runs on every Merge Request.
