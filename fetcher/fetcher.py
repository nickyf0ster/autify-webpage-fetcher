import argparse
import json
import os.path
import time
from datetime import datetime
from os import path

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.webdriver import WebDriver


class TinyDB:
    def __init__(self, db_file_path: str):
        self.db_file_path = db_file_path
        self.__init_db()

    def set(self, key: str, value: any):
        file_data = self.__read_file()
        file_data[key] = value
        self.__write_file(file_data)

    def get(self, key: str):
        return self.__read_file().get(key)

    def __init_db(self):
        if not path.exists(self.db_file_path):
            with open(self.db_file_path, 'a') as f:
                f.write(json.dumps({}))

    def __read_file(self) -> dict:
        with open(self.db_file_path, "r") as f:
            file_data = f.read()
        return json.loads(file_data)

    def __write_file(self, data):
        with open(self.db_file_path, "w") as f:
            f.write(json.dumps(data))


class WebFetcher:
    def __init__(self, driver_engine: str):
        self.db = TinyDB(db_file_path=os.path.dirname(__file__) + "/.db.json")
        self.driver = self.__init_driver(driver_engine)

    def save_page(self, url: str) -> None:
        """
        Saves page (with attributes) into corresponding .htlm file
        :param url: str, page's URL
        :return: None
        """

        page_html = self.__get_page_html(url)
        if page_html:
            print(f"Saving page {url}")
            soup = BeautifulSoup(page_html, "html.parser")
            filename = url.split("://")[1]
            with open(os.path.dirname(__file__) + f"/../{filename}.html", "w", encoding="utf-8") as f:
                f.write(soup.prettify())
            print(f"Successfully saved page {url}")

    def get_page_metadata(self, url) -> dict:
        meta_from_db = self.db.get(url)
        if meta_from_db:
            num_links = meta_from_db.get('num_links')
            images = meta_from_db.get('images')
            dt_object = datetime.fromtimestamp(meta_from_db.get('last_fetch'))
            last_fetch = dt_object.strftime("%a %b %d %Y %H:%M UTC")

            # Tue Mar 16 2021 15:46 UTC
            print(f"\nsite: {url}\nnum_links: {num_links}\nimages: {images}\nlast_fetch: {last_fetch}\n")
        else:
            print(f"No metadata for {url} yet. Consider fetching site first.")

    def quit(self):
        self.driver.quit()

    def __init_driver(self, webdriver_name: str = None) -> WebDriver:
        """
        Method for webdirver initialization
        :param webdriver_name: <str>, webdriver name, eg. Firefox
        :return: WebDriver object
        """
        print(f"Starting {webdriver_name} webdriver initialization")
        if webdriver_name == "Firefox":
            options = Options()
            options.headless = True
            options.add_argument("--no-sandbox")
            options.add_argument("--disable-dev-shm-usage")
            options.add_argument("--ignore-certificate-errors")
            driver = webdriver.Firefox(options=options, service_log_path=os.path.dirname(__file__) + "/geckodriver.log")
        else:
            raise NotImplementedError

        print(f"{webdriver_name} webdriver successfully initialized")
        return driver

    def __get_page_html(self, url: str) -> str:
        """
        Method for page HTML acquisition
        :param url: str
        :return: str, page HTML code
        """
        if self.__is_url_valid(url):
            try:
                self.driver.get(url)
                self.__update_page_metadata(url)
                self.driver.implicitly_wait(10)
                soup = BeautifulSoup(self.driver.page_source, features="html.parser")
                return soup.prettify()
            except WebDriverException as e:
                print(f"An error occured while trying to download url: {url}\n{e}")
        else:
            print(f"Invalid URL for {url}. Please, specify protocol (eg: https://example.com)")

    def __update_page_metadata(self, url):
        self.driver.refresh()
        meta = {
            "last_fetch": time.time(),
            "num_links": len(self.driver.find_elements_by_tag_name('a')),
            "images": len(self.driver.find_elements_by_tag_name('img'))
        }
        self.db.set(url, meta)

    def __is_url_valid(self, url: str) -> bool:
        """
        Check if URL matches "https[http]://" pattern
        :param url: str
        :return: bool, True if url is valid
        """
        if url.startswith("https://") or url.startswith("http://"):
            return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Fetch webpage data and store it locally")
    parser.add_argument("urls", type=str, nargs="+",
                        help="list of urls to process")
    parser.add_argument("--metadata", action="store_true",
                        help="specify to get page metadata")
    args = parser.parse_args()
    urls = args.urls
    get_meta = args.metadata

    fetcher = WebFetcher(driver_engine="Firefox")

    for url in urls:
        if get_meta:
            fetcher.get_page_metadata(url)
        else:
            fetcher.save_page(url)

    fetcher.quit()
